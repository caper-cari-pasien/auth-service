package com.caper.authservice.dto.response;

import com.caper.authservice.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class LoginResponse extends BaseResponse {
    String token;
    String username;
    UserDto user;
}
