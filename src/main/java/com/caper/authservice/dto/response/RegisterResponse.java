package com.caper.authservice.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class RegisterResponse extends BaseResponse {
    String username;
    String name;
}
