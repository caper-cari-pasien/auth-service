package com.caper.authservice.service;

import com.caper.authservice.dto.LoginDto;
import com.caper.authservice.dto.response.LoginResponse;
import com.caper.authservice.dto.response.RegisterResponse;
import com.caper.authservice.dto.UserDto;
import com.caper.authservice.exceptions.UserAlreadyExistException;
import com.caper.authservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImplementation implements AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public RegisterResponse register(UserDto request) {
        var checkUser = userRepository.findByUsername(request.getUsername());

        if (checkUser != null) {
            throw new UserAlreadyExistException("User already exists");
        }

        var user = UserDto.builder()
                .nama(request.getNama())
                .username(request.getUsername())
                .domisili(request.getDomisili())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .umur(request.getUmur())
                .build();

        userRepository.add(user);

        return RegisterResponse.builder().
                username(user.getUsername())
                .name(user.getNama())
                .build();
    }

    public LoginResponse login(LoginDto request) {
        UserDto user = userRepository.findByUsername(request.getUsername());
        if (user != null && passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            String token = jwtService.generateToken(user);

            user.setPassword("");
            return LoginResponse.builder()
                    .username(user.getUsername())
                    .token(token)
                    .user(user)
                    .build();
        }

        throw new BadCredentialsException("Username or password doesn't match");
    }
}
