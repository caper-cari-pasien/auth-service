package com.caper.authservice.service;

import com.caper.authservice.dto.LoginDto;
import com.caper.authservice.dto.response.LoginResponse;
import com.caper.authservice.dto.response.RegisterResponse;
import com.caper.authservice.dto.UserDto;

public interface AuthenticationService {
    RegisterResponse register(UserDto request);
    LoginResponse login(LoginDto request);
}
