package com.caper.authservice.exceptions.advice;

import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.caper.authservice.exceptions.ErrorTemplate;
import com.caper.authservice.exceptions.UserAlreadyExistException;


@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = { UserAlreadyExistException.class, BadCredentialsException.class })
    public ResponseEntity<Object> badRequest(Exception exception) {
        var httpStatus = HttpStatus.BAD_REQUEST;
        var baseException = ErrorTemplate.builder().message(exception.getMessage()).httpStatus(httpStatus).build();

        return new ResponseEntity<>(baseException, httpStatus);
    }

    @ExceptionHandler(value = { JwtException.class, UsernameNotFoundException.class })
    public ResponseEntity<Object> unauthorized(Exception exception) {
        var httpStatus = HttpStatus.UNAUTHORIZED;
        var baseException = ErrorTemplate.builder().message(exception.getMessage()).httpStatus(httpStatus).build();

        return new ResponseEntity<>(baseException, httpStatus);
    }

    @ExceptionHandler(value = { Exception.class })
    public ResponseEntity<Object> generalError(Exception exception) {
        var httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        var baseException = ErrorTemplate.builder().message(exception.getMessage()).httpStatus(httpStatus).build();

        return new ResponseEntity<>(baseException, httpStatus);
    }

}
