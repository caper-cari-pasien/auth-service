package com.caper.authservice.exceptions;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class ErrorTemplate {
    String message;
    HttpStatus httpStatus;
}