package com.caper.authservice.repository;

import com.caper.authservice.dto.UserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@Repository
public class UserRepository {
    private WebClient client;

    public UserRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/user");
    }

    public UserDto findByUsername(String username) {
        try{
            return client.get().uri("/name/" + username).retrieve().bodyToMono(UserDto.class).block();
        }catch (WebClientResponseException e){
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)){
                return null;
            }

            throw e;
        }
    }

    public void add(UserDto user) {
        client.post().uri("/add").body(Mono.just(user), UserDto.class).retrieve()
                .bodyToMono(UserDto.class).block();
    }
}
