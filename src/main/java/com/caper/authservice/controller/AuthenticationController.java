package com.caper.authservice.controller;

import com.caper.authservice.dto.LoginDto;
import com.caper.authservice.dto.response.BaseResponse;
import com.caper.authservice.dto.response.LoginResponse;
import com.caper.authservice.dto.response.RegisterResponse;
import com.caper.authservice.dto.UserDto;
import com.caper.authservice.service.AuthenticationService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(
            @RequestBody UserDto request,
            HttpServletResponse response
            ) {
        var responseBody = authenticationService.register(request);
        responseBody.setMessage("Successfully registered");
        response.setStatus(HttpStatus.CREATED.value());
        return ResponseEntity.status(HttpStatus.CREATED).body(responseBody);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(
            @RequestBody LoginDto request, HttpServletResponse response) {
        var responseBody = authenticationService.login(request);
        responseBody.setMessage("Successfully logged in");
        response.addCookie(createCookie("token", responseBody.getToken()));
        return ResponseEntity.status(HttpStatus.OK).body(responseBody);
    }

    @PostMapping("/logout")
    public ResponseEntity<BaseResponse> logout(HttpServletResponse response) {
        deleteCookie(response, "token");
        var responseBody = BaseResponse.builder().message("Successfully logged out").build();
        return ResponseEntity.status(HttpStatus.OK).body(responseBody);
    }

    private static Cookie createCookie(String cookieName, String value) {
        var newUrlEncoding = URLEncoder.encode(value, StandardCharsets.UTF_8);
        var oldUrlEncoding = newUrlEncoding.replace("+", "%20");
        var cookie = new Cookie(cookieName, oldUrlEncoding);
        cookie.setSecure(false);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(60 * 60 * 24 * 7);
        return cookie;
    }

    private static void deleteCookie(HttpServletResponse response, String cookieName) {
        var cookie = createCookie(cookieName, "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}
