package com.caper.authservice.utils;

import com.caper.authservice.dto.LoginDto;
import com.caper.authservice.dto.response.LoginResponse;
import com.caper.authservice.dto.response.RegisterResponse;
import com.caper.authservice.dto.UserDto;

public class TestUtils {
    public static UserDto createUserDto() {
        return UserDto.builder().nama("nama").username("username")
                .password("password").role("DOCTOR").umur(20).domisili("domisili").build();
    }

    public static LoginDto createLoginDto() {
        return LoginDto.builder().username("username").password("password").build();
    }

    public static RegisterResponse createRegisterResponse() {
        return RegisterResponse.builder().username("username").name("nama").build();
    }

    public static LoginResponse createLoginResponse() {
        return LoginResponse.builder().user(createUserDto()).token("token").username("username").build();
    }
}
