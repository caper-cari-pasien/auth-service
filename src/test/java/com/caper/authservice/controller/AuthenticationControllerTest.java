package com.caper.authservice.controller;

import com.caper.authservice.service.AuthenticationService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.caper.authservice.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AuthenticationControllerTest {
    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private HttpServletResponse servletResponse;

    @InjectMocks
    private AuthenticationController authenticationController;

    @Test
    void testRegisterShouldCallRegisterAndReturnResponse() {
        // set up
        var request = createUserDto();
        var response = createRegisterResponse();

        when(authenticationService.register(request)).thenReturn(response);

        doNothing().when(servletResponse).setStatus(HttpStatus.CREATED.value());
        doNothing().when(servletResponse).setContentType("application/json");

        // test
        var responseEntity = authenticationController.register(request, servletResponse);

        // assert
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(response, responseEntity.getBody());

        verify(authenticationService, times(1)).register(request);
        verify(servletResponse, times(1)).setStatus(HttpStatus.CREATED.value());
    }

    @Test
    void testLoginShouldCallLoginAndReturnResponseWithCookie() {
        // set up
        var request = createLoginDto();
        var response = createLoginResponse();
        var token = "token";

        when(authenticationService.login(request)).thenReturn(response);

        doNothing().when(servletResponse).addCookie(any(Cookie.class));

        // test
        var responseEntity = authenticationController.login(request, servletResponse);

        // assert
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(response, responseEntity.getBody());

        verify(authenticationService, times(1)).login(request);
        verify(servletResponse, times(1)).addCookie(any(Cookie.class));

        // assert cookie
        ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass(Cookie.class);
        verify(servletResponse, times(1)).addCookie(cookieCaptor.capture());

        Cookie cookie = cookieCaptor.getValue();
        assertNotNull(cookie);
        assertEquals("token", cookie.getName());
        assertEquals(token, cookie.getValue());
    }

    @Test
    void testLogoutShouldDeleteCookieAndReturnResponse() {
        // set up
        doNothing().when(servletResponse).addCookie(any(Cookie.class));

        // test
        var responseEntity = authenticationController.logout(servletResponse);

        // assert
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        verify(servletResponse, times(1)).addCookie(any(Cookie.class));

        // assert cookie
        ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass(Cookie.class);
        verify(servletResponse, times(1)).addCookie(cookieCaptor.capture());

        Cookie cookie = cookieCaptor.getValue();
        assertNotNull(cookie);
        assertEquals("token", cookie.getName());
        assertEquals("", cookie.getValue());
        assertEquals(0, cookie.getMaxAge());
    }
}
