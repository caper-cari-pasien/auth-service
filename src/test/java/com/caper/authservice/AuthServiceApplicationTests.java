package com.caper.authservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class AuthServiceApplicationTests {

    @Test
    void testApplicationStartsWithoutExceptions() {
        assertDoesNotThrow(() -> AuthServiceApplication.main(new String[]{}));
    }
}
