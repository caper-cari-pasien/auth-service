package com.caper.authservice.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.io.IOException;

import static com.caper.authservice.utils.TestUtils.createUserDto;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@ExtendWith(SpringExtension.class)
class UserRepositoryTest {
    private static MockWebServer mockWebServer;

    private UserRepository userRepository;

    @BeforeEach
    void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        String baseUrl = String.format("http://localhost:%s",
                mockWebServer.getPort());
        userRepository = new UserRepository(baseUrl);
    }

    @AfterEach
    void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void testFindByUsernameShouldReturnUserDto() throws Exception {
        // set up
        var username = "username";
        var expectedUserDto = createUserDto();
        mockWebServer.enqueue(new MockResponse().setBody(new ObjectMapper().writeValueAsString(expectedUserDto))
                .addHeader("Content-Type", "application/json"));

        // test
        var actualUserDto = userRepository.findByUsername(username);

        // assert
        assertNotNull(actualUserDto);
        assertEquals(expectedUserDto, actualUserDto);
    }

    @Test
    void testFindByInvalidUsernameShouldReturnNull() {
        // set up
        var username = "username";
        mockWebServer.enqueue(new MockResponse().setResponseCode(404).setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // test
        var actualUserDto = userRepository.findByUsername(username);

        // assert
        assertNull(actualUserDto);
    }

    @Test
    void testUnhandledErrorShouldThrowException() {
        // set up
        var username = "username";
        mockWebServer.enqueue(new MockResponse().setResponseCode(500).setBody("{}")
                .addHeader("Content-Type", "application/json"));

        // assert
        assertThrows(WebClientResponseException.class, () -> userRepository.findByUsername(username));
    }

    @Test
    void testAddUserShouldSuccess() throws Exception {
        var userDto = createUserDto();
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(new ObjectMapper().writeValueAsString(userDto))
                .addHeader("Content-Type", "application/json"));
        assertDoesNotThrow(() -> userRepository.add(userDto));
    }
}
