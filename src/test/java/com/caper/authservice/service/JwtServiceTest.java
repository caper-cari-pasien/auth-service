package com.caper.authservice.service;

import com.caper.authservice.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;

import static com.caper.authservice.utils.TestUtils.createUserDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class JwtServiceTest {
    @InjectMocks
    JwtService jwtService;

    @Test
    void testGenerateTokenShouldReturnToken() {
        // set up
        var userDetails = createUserDto();

        // test
        var token = jwtService.generateToken(userDetails);

        // assert
        assertNotNull(token);
    }
}
