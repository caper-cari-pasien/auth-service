package com.caper.authservice.service;

import com.caper.authservice.exceptions.UserAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.caper.authservice.dto.UserDto;
import com.caper.authservice.repository.UserRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.caper.authservice.utils.TestUtils.createLoginDto;
import static com.caper.authservice.utils.TestUtils.createUserDto;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AuthenticationServiceTest {
    @Mock
    UserRepository userRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    JwtService jwtService;

    @InjectMocks
    AuthenticationServiceImplementation authenticationService;

    @Test
    void testRegisterShouldAddToRepository() {
        // set up
        var request = createUserDto();

        when(userRepository.findByUsername(request.getUsername())).thenReturn(null);
        when(passwordEncoder.encode(request.getPassword())).thenReturn("encodedPassword");

        // test
        var response = authenticationService.register(request);

        // assert
        assertNotNull(response);
        assertEquals(request.getUsername(), response.getUsername());
        assertEquals(request.getNama(), response.getName());
        verify(userRepository, times(1)).add(any(UserDto.class));
    }

    @Test
    void testRegisterShouldThrowUserAlreadyExistsException() {
        // set up
        var request = createUserDto();

        when(userRepository.findByUsername(request.getUsername())).thenReturn(createUserDto());

        // assert
        assertThrows(UserAlreadyExistException.class, () -> authenticationService.register(request));
        verify(userRepository, never()).add(any(UserDto.class));
    }

    @Test
    void testLoginShouldReturnLoginResponse() {
        // set up
        var request = createLoginDto();
        var user = createUserDto();
        var userPassword = user.getPassword();
        var token = "token";

        when(userRepository.findByUsername(request.getUsername())).thenReturn(user);
        when(passwordEncoder.matches(request.getPassword(), userPassword)).thenReturn(true);
        when(jwtService.generateToken(user)).thenReturn(token);

        // test
        var response = authenticationService.login(request);

        // assert
        assertNotNull(response);
        assertEquals(user.getUsername(), response.getUsername());
        assertEquals(token, response.getToken());
        assertEquals(user, response.getUser());
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(passwordEncoder, times(1)).matches(request.getPassword(), userPassword);
        verify(jwtService, times(1)).generateToken(user);
    }

    @Test
    void testLoginWithInvalidUsernameShouldThrowBadCredentialsException() {
        // set up
        var request = createLoginDto();

        when(userRepository.findByUsername(request.getUsername())).thenReturn(null);

        // assert
        assertThrows(BadCredentialsException.class, () -> authenticationService.login(request));
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(passwordEncoder, never()).matches(anyString(), anyString());
        verify(jwtService, never()).generateToken(any(UserDto.class));
    }

    @Test
    void testLoginWithInvalidPasswordShouldThrowBadCredentialsException() {
        // set up
        var request = createLoginDto();
        var user = createUserDto();
        request.setPassword("abcde");

        when(userRepository.findByUsername(request.getUsername())).thenReturn(user);
        when(passwordEncoder.matches(request.getPassword(), user.getPassword())).thenReturn(false);

        // assert
        assertThrows(BadCredentialsException.class, () -> authenticationService.login(request));
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(passwordEncoder, times(1)).matches(anyString(), anyString());
        verify(jwtService, never()).generateToken(any(UserDto.class));
    }
}
