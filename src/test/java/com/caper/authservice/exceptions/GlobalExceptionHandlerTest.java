package com.caper.authservice.exceptions;

import com.caper.authservice.exceptions.advice.GlobalExceptionHandler;
import io.jsonwebtoken.JwtException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class GlobalExceptionHandlerTest {
    private GlobalExceptionHandler exceptionHandler;

    @BeforeEach
    void setUp() {
        exceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    void testUserAlreadyExistExceptionHandler() {
        // set up
        var message = "User already exists";
        var exception = new UserAlreadyExistException(message);

        // test
        var response = exceptionHandler.badRequest(exception);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(message, ((ErrorTemplate) response.getBody()).getMessage());
    }

    @Test
    void testBadCredentialsExceptionHandler() {
        // set up
        var message = "Invalid credentials";
        var exception = new BadCredentialsException(message);

        // test
        var response = exceptionHandler.badRequest(exception);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(message, ((ErrorTemplate) response.getBody()).getMessage());
    }

    @Test
    void testJwtExceptionHandler() {
        // set up
        var message = "Invalid token";
        var exception = new JwtException(message);

        // test
        var response = exceptionHandler.unauthorized(exception);

        // assert
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertEquals(message, ((ErrorTemplate) response.getBody()).getMessage());
    }

    @Test
    void testUsernameNotFoundExceptionHandler() {
        // set up
        var message = "Username not found";
        var exception = new UsernameNotFoundException(message);

        // test
        var response = exceptionHandler.unauthorized(exception);

        // assert
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertEquals(message, ((ErrorTemplate) response.getBody()).getMessage());
    }

    @Test
    void testGeneralExceptionHandler() {
        // set up
        var message = "Unhandled exception";
        var exception = new Exception(message);

        // test
        var response = exceptionHandler.generalError(exception);

        // assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(message, ((ErrorTemplate) response.getBody()).getMessage());
    }
}
